from SPARQLWrapper import SPARQLWrapper, JSON


WIKIDATA_ENDPOINT = 'https://query.wikidata.org/sparql'
HTTP_AGENT_HEADER = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 ' \
                    'Safari/537.11 '

IN_COUNTRY = 'P17'
IMAGE = 'P18'
INSTANCE_OF = 'P31'
PCP_PREFERENCE_NUMBER = 'P381'
COORDINATE_LOCATION = 'P625'
HERITAGE_DESIGNATION = 'P1435'

SWITERLAND = 'Q39'
CASTLE = 'Q23413'
FORTIFICATION = 'Q57821'
CASTRUM = 'Q88205'
CHATEAU = 'Q751876'
ARCHITECTURAL_STRUCTURE = 'Q811979'
MANSION = 'Q1802963'
CASTLE_RUIN = 'Q17715832'
DEFENSIVE_TOWER = 'Q20034791'

QUERY = f'''SELECT ?item ?itemLabel ?coord ?KGS ?designation
WHERE {{
  {{
    ?item wdt:{IN_COUNTRY} wd:{SWITERLAND}.
    ?item wdt:{COORDINATE_LOCATION} ?coord.
    {{
      ?item wdt:{INSTANCE_OF} wd:{CASTLE}.
    }}
    UNION
    {{
      ?item wdt:{INSTANCE_OF} wd:{CHATEAU}.
    }}
    UNION
    {{
      ?item wdt:{INSTANCE_OF} wd:{CASTLE_RUIN}.
    }}
    UNION
    {{
      ?item wdt:{INSTANCE_OF} wd:{MANSION}.
    }}
    UNION
    {{
      ?item wdt:{INSTANCE_OF} wd:{CASTRUM}.
    }}
    UNION
    {{
      ?item wdt:{INSTANCE_OF} wd:{DEFENSIVE_TOWER}.
    }}
    UNION
    {{
      ?item wdt:{INSTANCE_OF} wd:{ARCHITECTURAL_STRUCTURE}.
      ?item wdt:{INSTANCE_OF} wd:{FORTIFICATION}.
    }}
    FILTER NOT EXISTS
    {{
      ?item wdt:{IMAGE} ?pic.
    }}
    OPTIONAL
    {{
      ?item wdt:{PCP_PREFERENCE_NUMBER} ?KGS.
    }}
    OPTIONAL
    {{
      ?item wdt:{HERITAGE_DESIGNATION} ?designation.
    }}
  }}
  SERVICE wikibase:label {{ bd:serviceParam wikibase:language "[AUTO_LANGUAGE],de,fr,en,it,rm" }}
}}
ORDER BY ASC(?item)'''


def execute_query():
    sparql = SPARQLWrapper(WIKIDATA_ENDPOINT, agent=HTTP_AGENT_HEADER)
    sparql.setQuery(QUERY)
    sparql.setReturnFormat(JSON)
    return sparql.query().convert()

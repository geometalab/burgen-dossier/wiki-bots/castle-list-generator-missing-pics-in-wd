from missing_picture_processor import query_and_process_castles
from add_text_to_wiki import add_text_to_wiki

FILEPATH = 'wiki'
TEXTFILE = 'wd_missing_pics'
WIKIPAGE = u'Wikimedia_CH/Burgen-Dossier/FehlendeFotosinWikidatabzwCommons'


def main():
    query_and_process_castles(FILEPATH, TEXTFILE)
    add_text_to_wiki(f'{FILEPATH}/{TEXTFILE}', WIKIPAGE)


if __name__ == "__main__":
    main()

import os

import requests
from wikidata_query import execute_query

CANTONS = {
    "Bern": "BE",
    "Berne": "BE",
    "Luzern": "LU",
    "Uri": "UR",
    "Schwyz": "SZ",
    "Obwalden": "OW",
    "Nidwalden": "NW",
    "Glarus": "GL",
    "Zug": "ZG",
    "Fribourg": "FR",
    "Freiburg": "FR",
    "Solothurn": "SO",
    "Basel-Stadt": "BS",
    "Basel-Landschaft": "BL",
    "Schaffhausen": "SH",
    "Appenzell Ausserrhoden": "AR",
    "Appenzell Innerrhoden": "AI",
    "Sankt Gallen": "SG",
    "Graubünden": "GR",
    "Grigioni": "GR",
    "Grischun": "GR",
    "Aargau": "AG",
    "Thurgau": "TG",
    "Ticino": "TI",
    "Vaud": "VD",
    "Valais": "VS",
    "Wallis": "VS",
    "Neuchâtel": "NE",
    "Genève": "GE",
    "Jura": "JU",
    "Zürich": "ZH",
}

QNUMBER_CATEGORY = {
    "http://www.wikidata.org/entity/Q8274529": "A",
    "http://www.wikidata.org/entity/Q12126757": "B",
}

CASTLE_MAP_QUERY_URL = "https://castle-map.infs.ch/#"
ZOOM_LEVEL = 17

NOMINATIM_QUERY_URL = "https://nominatim.openstreetmap.org/reverse?format=json&"

OSM_TYPE_MAPPING = dict(node="N", way="W", relation="R")


def _get_first_key_or_unknown(d: dict, keys: list):
    for key in keys:
        if key in d:
            return d[key]
    return "unknown"


def _get_castle_data():
    castle_data = execute_query()
    return _create_castle_list(castle_data)


def _create_castle_list(castle_data):
    data = [castle_data["results"]["bindings"][0]]

    for binding in castle_data["results"]["bindings"]:
        already_contained = False
        for candidate in data:
            if (
                binding["item"]["value"].split("/")[4]
                == candidate["item"]["value"].split("/")[4]
            ):
                already_contained = True
        if not already_contained:
            data.append(binding)

    return data


def _process_data_entry(data, file):
    coord = data["coord"]["value"].replace("Point(", "").replace(")", "").split(" ")

    castle_address_request = requests.get(
        f"{NOMINATIM_QUERY_URL}lat={coord[1]}&lon={coord[0]}"
        f"&zoom=10&addressdetails=1"
    )
    castle_address = castle_address_request.json()

    castle_osm_entry_request = requests.get(
        f"{NOMINATIM_QUERY_URL}lat={coord[1]}&lon={coord[0]}"
    )
    castle_osm_entry = castle_osm_entry_request.json()

    rsid = "-"
    category = "-"

    if "KGS" in data:
        rsid = data["KGS"]["value"]

    if "designation" in data:
        designation = data["designation"]["value"]
        category = QNUMBER_CATEGORY.get(designation, designation)

    castle_url = f"""{CASTLE_MAP_QUERY_URL}{coord[1]},{coord[0]},{ZOOM_LEVEL}z,"""
    castle_identifier = f"""{OSM_TYPE_MAPPING[(castle_osm_entry['osm_type'])]}{castle_osm_entry['osm_id']}"""
    _write(
        file,
        f"{castle_url}{castle_identifier}",
        data["item"]["value"],
        data["itemLabel"]["value"],
        category,
        _get_first_key_or_unknown(
            castle_address["address"],
            ["city", "municipality", "city", "town", "village"],
        ),
        coord[1],
        _get_first_key_or_unknown(CANTONS, castle_address["address"]["state"].split("/")),
        coord[0],
        rsid,
        data["item"]["value"].split("/")[4],
    )


def _write(
    file,
    map_link,
    object_link,
    object_name,
    cat,
    town,
    xcoord,
    kuerzel,
    ycoord,
    kgsnr,
    wikidata,
):
    file.write(
        f"{{{{User:Chilfing/template_row|Burgenkarte={map_link}|Object=[{object_link} {object_name}]|"
        f"Category={cat}|Town={town}|Latitude={xcoord}|Region-ISO=CH-{kuerzel}|Longitude={ycoord}|"
        f"KGS-Nr={kgsnr}|Wikidata={wikidata}}}}}\n"
    )


def _process_data(data, file):
    for data_entry in data:
        _process_data_entry(data_entry, file)


def query_and_process_castles(path, file):
    if not os.path.exists(path):
        os.mkdir(path, 0o755)

    with open(f"{path}/{file}", "w+", encoding="utf8") as wiki_entry_file:
        wiki_entry_file.write(
            "== Wikidata-Datenobjekte ohne Bilder (mit Namen der Burgen und Schlösser ohne Bild) "
            "==\n"
        )
        wiki_entry_file.write(
            "{{Caution|Diese Seite wird automatisch mittels eines alle 2 Stunden ablaufenden "
            "Scripts aktualisiert. Das manuelle Updaten von Tabelleneinträgen lohnt sich deshalb "
            "nicht, weil Änderungen wieder überschrieben werden. Sollten Probleme mit dem Skript "
            "auftreten, so können Issues im ["
            "https://gitlab.com/geometalab/castle-list-generator-missing-pics-in-wd Repository] "
            "erfasst werden.}}"
        )
        wiki_entry_file.write("{{User:Chilfing/template_header}}\n")

        _process_data(_get_castle_data(), wiki_entry_file)

        wiki_entry_file.write("{{User:Chilfing/template_footer}}")
        wiki_entry_file.close()

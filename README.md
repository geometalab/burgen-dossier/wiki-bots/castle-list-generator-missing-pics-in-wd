# Wikidata Missing Pics Query

[![build status](https://gitlab.com/geometalab/castle-list-generator-missing-pics-in-wd/badges/master/pipeline.svg)](https://gitlab.com/geometalab/castle-list-generator-missing-pics-in-wd/commits/master)

The scripts in this repository query OSM for castles with missing Wikidata pictures and publish the results [here](https://meta.wikimedia.org/wiki/Wikimedia_CH/Burgen-Dossier/FehlendeFotosinWikidatabzwCommons).

## Overview

In the following, the scripts involved in the process are listed and briefly described to provide an overview of the application.

* **castle_processing.py**: This script queries the [Wikidata Sparql Endpoint](https://query.wikidata.org/) for `Q23413(castle)`, `Q751876(château)`, `Q17715832(castle ruin)` and `Q1802963(mansion)` and makes a file containing the formatted information.

* **add\_text\_to\_wiki.py**: This script publishes the contents of the previously produced file on [Wikimedia](https://meta.wikimedia.org/wiki/Wikimedia_CH/Burgen-Dossier/FehlendeFotosinWikidatabzwCommons) using a shared [PyWikiBot](https://www.mediawiki.org/wiki/Manual:Pywikibot) with username `MissingCastlePicturesBot`. If the bots behaviour causes issues, please create an issue on this repository's issue page.

* **main.py**: This script executes the above features in the order described. The CI job for this repository is scheduled to run every hour on `HH:20`. The linked artifacts are updated according to this schedule.

## Contribution
If you think you have a valuable contribution, feel free to open a pull request.